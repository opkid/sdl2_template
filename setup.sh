# git submodule update
git submodule init
git submodule update

# android project setup
cd Android/app/jni
ln -sf ../../../Frameworks/SDL2 ./SDL2
ln -sf ../../../Frameworks/SDL2_image ./SDL2_image
ln -sf ../../../Frameworks/SDL2_mixer ./SDL2_mixer
ln -sf ../../../Source ./Source
cd ../../../
