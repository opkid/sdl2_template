#include "SDL.h"

SDL_Window* window;
SDL_Renderer* renderer;
bool started = false;

void clean();
void start();

int main(int argc, char* argv[]) {

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        SDL_Log("%s", SDL_GetError());
        clean();
    }

    window = SDL_CreateWindow(
        "Example",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        480,
        640,
        SDL_WINDOW_FULLSCREEN
    );
    if (window == NULL) {
        SDL_Log("%s", SDL_GetError());
        clean();
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        SDL_Log("%s", SDL_GetError());
        clean();
    }

    start();
    return 0;
}

void tick () {
    SDL_Event event;
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_Rect windowRect;
    windowRect.x = 0;
    windowRect.y = 0;
    windowRect.w = 480;
    windowRect.h = 640;

    while(started) {
        SDL_PollEvent(&event);

        switch (event.type)
        {
        case SDL_QUIT:
            started = false;
            break;
        default:
            break;
        }
        SDL_RenderClear(renderer);
        SDL_RenderFillRect(renderer, &windowRect);
        SDL_RenderPresent(renderer);
    }
    clean();
}

void start() {
    started = true;
    tick();
}

void clean() {
    int returnCode = 0;

    if (window != NULL) {
        SDL_DestroyWindow(window);
    } else returnCode++;

    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
    } else returnCode++;

    SDL_Quit();
    exit(returnCode);
}
